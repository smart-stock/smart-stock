package com.smartitengineering.stockexchange.feeds.ws;

import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import com.smartitengineering.stockexchange.feeds.ws.domain.JsonProvider;
import com.smartitengineering.stockexchange.feeds.ws.domain.QuotesResource;
import com.smartitengineering.stockexchange.feeds.ws.domain.StockQuote;
import com.sun.syndication.io.WireFeedOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
@Path("/api/exchange")
public class LatestQuotesForCompaniesResource {

  private static final Logger log = Logger.getLogger(
      LatestQuotesForCompaniesResource.class.getName());
  private static final String ALL_PATTERN_STR =
                              "([a-zA-Z0-9_]+)(-[a-z]+)?(\\.[a-z]+)";
  private static final Pattern ALL_PATTERN = Pattern.compile(ALL_PATTERN_STR);
  private static final MediaType DEFAULT = MediaType.APPLICATION_JSON_TYPE;
  private final UriInfo uriInfo;

  public LatestQuotesForCompaniesResource(@Context UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }

  protected Response getString(String exchangeId,
                               MediaType type,
                               String languageId,
                               Set<String> quoteTickrs) {
    if (type == null) {
      type = DEFAULT;
    }
    final Response response;
    if (quoteTickrs == null || quoteTickrs.isEmpty()) {
      response = Response.status(Response.Status.BAD_REQUEST).build();
    }
    else {
      if (type.equals(DEFAULT) || type.equals(MediaTypeExtensionEnum.rss.
          getMediaType())) {
        response = generate(quoteTickrs, exchangeId, type);
      }
      else {
        response =
        Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
      }
    }
    return response;
  }

  @GET
  @Path("{exchangeId: [a-zA-Z0-9_]+}/{type: [a-z]+}/{language: [a-z]+}")
  public Response getString(@PathParam("exchangeId") String exchangeId,
                            @PathParam("type") String typeId,
                            @PathParam("language") String languageId,
                            @QueryParam("tickr") Set<String> quoteTickrs) {
    if (log.isLoggable(Level.FINER)) {
      log.finer(String.format(
          "Path param accessor with %s exchange id, %s type id, %s language and %s quoteTickrs..",
          exchangeId, typeId, languageId, quoteTickrs));
    }
    MediaType type;
    if (StringUtils.isBlank(typeId)) {
      type = DEFAULT;
    }
    else {
      try {
        type = MediaTypeExtensionEnum.valueOf(typeId).getMediaType();
      }
      catch (Exception ex) {
        try {
          type = MediaType.valueOf(typeId);
        }
        catch (Exception exc) {
          type = DEFAULT;
        }
      }
    }
    return getString(exchangeId, type, languageId, quoteTickrs);
  }

  @GET
  @Path("{exchangeId: [a-zA-Z0-9_]+}/{type: [a-z]+}")
  public Response getString(@PathParam("exchangeId") String exchangeId,
                            @QueryParam("lang") String languageParam,
                            @HeaderParam("Language") String languageHeader,
                            @PathParam("type") String typeId,
                            @QueryParam("tickr") Set<String> quoteTickrs) {
    if (log.isLoggable(Level.FINER)) {
      log.finer(String.format("Path+Query param accessor with %s exchange id, %s type id, %s language header, %s lang " +
                              "query param and %s quoteTickrs..", exchangeId,
          typeId, languageHeader, languageParam, quoteTickrs));
    }
    String langId = languageParam == null ? languageHeader == null ? "" : languageHeader
                    : languageParam;
    return getString(exchangeId, typeId, langId, quoteTickrs);
  }

  @GET
  @Path("{exchangeSpec: " + ALL_PATTERN_STR + "}")
  public Response getStringWithSpecInSinglePath(
      @QueryParam("lang") String languageParam,
      @HeaderParam("Language") String languageHeader,
      @PathParam("exchangeSpec") final String exchangeSpec,
      @QueryParam("tickr") Set<String> quoteTickrs) {
    if (log.isLoggable(Level.FINER)) {
      log.finer(String.format(
          "URI accessor with %s URI spec, %s language header, %s lang and %s quoteTickrs..",
          exchangeSpec, languageHeader, languageParam, quoteTickrs));
    }
    Matcher allMatcher = ALL_PATTERN.matcher(exchangeSpec);
    String exchangeId, langId, typeId;
    if (allMatcher.find()) {
      exchangeId = allMatcher.group(1);
      langId = allMatcher.group(2);
      typeId = allMatcher.group(3);
      if (StringUtils.isNotEmpty(langId)) {
        langId = langId.substring(1);
      }
      else {
        langId = languageParam == null ? languageHeader == null ? "" : languageHeader
                 : languageParam;
      }
      if (StringUtils.isNotEmpty(typeId)) {
        typeId = typeId.substring(1);
      }
      else {
        typeId = "";
      }
    }
    else {
      exchangeId = langId = typeId = null;
    }
    return getString(exchangeId, typeId, langId, quoteTickrs);
  }

  @GET
  @Path("{exchangeId: [a-zA-Z0-9_]+}")
  public Response getStringWithSpecInHeaderOrQueryParam(
      @PathParam("exchangeId") String exchangeId,
      @QueryParam("accept") String acceptParam,
      @QueryParam("lang") String languageParam,
      @HeaderParam("Accept") String acceptHeader,
      @HeaderParam("Language") String languageHeader,
      @QueryParam("tickr") Set<String> quoteTickrs) {
    if (log.isLoggable(Level.FINER)) {
      log.finer(String.format("Query param accessor with %s exchange id, %s language header, %s lang, %s accept header" +
                              ", %s accept and %s quoteTickrs..", exchangeId,
          languageHeader, languageParam, acceptHeader, acceptParam,
          quoteTickrs));
    }
    return getString(
        exchangeId,
        acceptParam != null ? acceptParam : acceptHeader,
        languageParam == null ? languageHeader : languageParam,
        quoteTickrs);
  }

  private Response generate(Set<String> quoteTickrs,
                            String exchangeId,
                            MediaType type) {
    if (type.toString().equals(MediaType.APPLICATION_JSON)) {
      return generateJson(quoteTickrs, exchangeId, type);
    }
    if (type.toString().equals(MediaTypeExtensionEnum.rss.getMediaType().
        toString())) {
      return generateRss(quoteTickrs, exchangeId, type);
    }
    else {
      return Response.status(Status.UNSUPPORTED_MEDIA_TYPE).build();
    }
  }

  private Response generateJson(Set<String> quoteTickrs,
                                String exchangeId,
                                MediaType type) {
    Response response;
    List<StockQuote> quotes =
                     new ArrayList<StockQuote>(quoteTickrs.size());
    final Status status = getQuotes(exchangeId, quoteTickrs, quotes);
    ResponseBuilder responseBuilder =
                    Response.status(Response.Status.OK);
    if (status.equals(Status.OK)) {
      QuotesResource resource =
                     new QuotesResource(quotes);
      GenericEntity<QuotesResource> quotesEntity =
                                    new GenericEntity<QuotesResource>(resource,
          QuotesResource.class);
      try {
        if (resource instanceof JsonProvider) {
          String json = resource.toJSON();
          responseBuilder.entity(json);
        }
        else {
          responseBuilder.entity(quotesEntity);
        }
        responseBuilder.type(type);
      }
      catch (Exception ex) {
        log.log(Level.WARNING, ex.getMessage(), ex);
        responseBuilder.status(Status.INTERNAL_SERVER_ERROR);
      }
    }
    else {
      responseBuilder.status(status);
    }
    response = responseBuilder.build();
    return response;
  }

  private Response generateRss(Set<String> quoteTickrs,
                               String exchangeId,
                               MediaType type) {
    Response response;
    List<StockQuote> quotes =
                     new ArrayList<StockQuote>(quoteTickrs.size());
    final Status status = getQuotes(exchangeId, quoteTickrs, quotes);
    ResponseBuilder responseBuilder =
                    Response.status(Response.Status.OK);
    if (status.equals(Status.OK)) {
      try {
        QuotesResource resource =
                       new QuotesResource(quotes);
        responseBuilder.entity(new WireFeedOutput().outputString(resource.
            toChannel(uriInfo)));
        responseBuilder.type(type);
      }
      catch (Exception ex) {
        log.log(Level.WARNING, ex.getMessage(), ex);
        responseBuilder.status(Status.INTERNAL_SERVER_ERROR);
      }
    }
    else {
      responseBuilder.status(status);
    }
    response = responseBuilder.build();
    return response;
  }

  private Status getQuotes(String exchangeId,
                           Set<String> quoteTickrs,
                           List<StockQuote> quotes) {
    try {
      StockExchange exchange = SmartStockFactory.getQuotesService().
          getStockExchange(exchangeId);
      Map<Company, com.smartitengineering.stockexchange.api.StockQuote> quotesMap = SmartStockFactory.getAPI().
          getQuotesService().getQuotesMapForStockExchangeFromPersistentStorage(
          exchange);
      for (String tickr : quoteTickrs) {
        try {
          Company company = SmartStockFactory.getQuotesService().getCompany(
              tickr, exchange);
          StockQuote quote = new StockQuote(quotesMap.get(company));
          quotes.add(quote);
        }
        catch (Exception ex) {
          log.log(Level.WARNING, ex.getMessage(), ex);
          return Status.BAD_REQUEST;
        }
      }
      return Status.OK;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      return Status.NOT_FOUND;
    }
  }
}
