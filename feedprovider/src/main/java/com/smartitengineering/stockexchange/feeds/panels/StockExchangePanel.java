package com.smartitengineering.stockexchange.feeds.panels;

import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.feeds.common.ParamEnum;
import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import com.smartitengineering.stockexchange.feeds.pages.StockExchangePage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author imyousuf
 */
public class StockExchangePanel
    extends Panel {

  public StockExchangePanel(String id) {
    super(id);
    initComponents();
  }

  protected void initComponents() {
    final Collection<StockExchange> stockExchanges =
        SmartStockFactory.getQuotesService().getStockExchanges();
    final Collection<StockExchange> exchanges = stockExchanges == null ? Collections.<StockExchange>
        emptyList() : stockExchanges;
    ListView<StockExchange> stockExchangesLoop = new ListView<StockExchange>(
        "exchange", new ArrayList<StockExchange>(exchanges)) {

      @Override
      protected void populateItem(ListItem<StockExchange> listItem) {
        StockExchange exchange = listItem.getModelObject();
        Map<String, String> param = new HashMap<String, String>();
        param.put(ParamEnum.STOCK_EXCHANGE_PARAM.getParamName(), exchange.
            getSymbolicName());
        final BookmarkablePageLink stockExchangeLink = new BookmarkablePageLink(
            "seLink", StockExchangePage.class, new PageParameters(param));
        listItem.add(stockExchangeLink);
        stockExchangeLink.add(new Label("name", exchange.getName()));
      }
    };
    add(stockExchangesLoop);
  }
}
