package com.smartitengineering.stockexchange.feeds.factory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 *
 * @author imyousuf
 */
public class SmartStockGuiceListener
    extends GuiceServletContextListener {

  @Override
  protected Injector getInjector() {
    final Injector injector = Guice.createInjector(Stage.PRODUCTION, new SmartStockWebAppModule());
    SmartStockFactory.setGuiceInjector(injector);
    return injector;
  }
}
