package com.smartitengineering.stockexchange.feeds.ws.domain;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

/**
 *
 * @author imyousuf
 */
public class StockExchange
    implements JsonProvider {

  public static final String PROP_QUOTES = "quotes";
  private final com.smartitengineering.stockexchange.api.StockExchange exchange;
  private final Collection<StockQuote> quotes;

  public StockExchange(
      com.smartitengineering.stockexchange.api.StockExchange exchange) {
    this.exchange = exchange;
    quotes = new ArrayList<StockQuote>();
  }

  public String getWebsiteUrl() {
    return exchange.getWebsiteUrl();
  }

  public String getTradingStartTime() {
    return exchange.getTradingStartTime();
  }

  public String getTradingEndTime() {
    return exchange.getTradingEndTime();
  }

  public String getSymbolicName() {
    return exchange.getSymbolicName();
  }

  public String getName() {
    return exchange.getName();
  }

  public String getAddress() {
    return exchange.getAddress();
  }

  public void addStockQuote(StockQuote quote) {
    quotes.add(quote);
  }

  public void removeStockQuote(StockQuote quote) {
    quotes.remove(quote);
  }

  public Collection<StockQuote> getQuotes() {
    return quotes;
  }

  public com.smartitengineering.stockexchange.api.StockExchange toExchange() {
    return exchange;
  }

  public void toJSON(final Writer writer,
                     final JsonGenerator parentGenerator)
      throws IOException {
    JsonFactory factory = new JsonFactory();
    final JsonGenerator generator;
    if (parentGenerator == null) {
      generator = factory.createJsonGenerator(writer);
    }
    else {
      generator = parentGenerator;
    }
    generator.writeStartObject();
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.StockExchange.PROP_NAME,
        getName());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.StockExchange.PROP_SYMBOLIC_NAME,
        getSymbolicName());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.StockExchange.PROP_TRADING_START_TIME,
        getTradingStartTime());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.StockExchange.PROP_TRADING_END_TIME,
        getTradingEndTime());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.StockExchange.PROP_WEB_URL,
        getWebsiteUrl());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.StockExchange.PROP_ADDRESS,
        getAddress());
    generator.writeArrayFieldStart(PROP_QUOTES);
    Iterator<StockQuote> quotesIterator = quotes.iterator();
    boolean start = true;
    while (quotesIterator.hasNext()) {
      if (!start) {
        generator.writeRaw(',');
      }
      else {
        start = false;
      }
      StockQuote quote = quotesIterator.next();
      generator.writeRaw(quote.toJSON());

    }
    generator.writeEndArray();
    generator.writeEndObject();
    if (parentGenerator == null) {
      generator.flush();
      generator.close();
    }
  }

  public String toJSON()
      throws IOException {
    StringWriter writer = new StringWriter();
    toJSON(writer, null);
    return writer.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final StockExchange other = (StockExchange) obj;
    if (this.exchange != other.exchange &&
        (this.exchange == null || !this.exchange.equals(other.exchange))) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 19 * hash + (this.exchange != null ? this.exchange.hashCode() : 0);
    return hash;
  }
}
