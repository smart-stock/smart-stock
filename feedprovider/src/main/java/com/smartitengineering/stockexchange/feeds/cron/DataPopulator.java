package com.smartitengineering.stockexchange.feeds.cron;

import com.google.inject.Singleton;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author imyousuf
 */
@Singleton
public class DataPopulator
    extends HttpServlet {

  private static final Logger log = Logger.getLogger(
      DataPopulator.class.getName());

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request,
                                HttpServletResponse response)
      throws ServletException,
             IOException {
    response.setStatus(HttpServletResponse.SC_OK);
    log.info("Getting new data for stock quotes for stock exchanges..");
    Collection<StockExchange> exchanges = SmartStockFactory.getQuotesService().
        getStockExchanges();
    //A call to initialize and persist registered stock exchanges
    SmartStockFactory.getLatestQuoteService();
    if (log.isLoggable(Level.INFO)) {
      log.log(Level.INFO, new StringBuilder("Got ").append(exchanges).append(
          " for pinging").toString());
    }
    if (exchanges != null) {
      for (StockExchange exchange : exchanges) {
        Collection<StockQuote> quotes = SmartStockFactory.getLatestQuoteService().
            getLatestStockQuotesFromExternalSource(exchange, true);
        if (log.isLoggable(Level.INFO)) {
          log.log(Level.INFO, new StringBuilder("Got ").append(quotes).append(
              " for ").append(exchange.getSymbolicName()).toString());
        }
      }
    }
    else {
      log.info("No data update!");
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request,
                       HttpServletResponse response)
      throws ServletException,
             IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request,
                        HttpServletResponse response)
      throws ServletException,
             IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}
