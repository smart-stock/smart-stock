package com.smartitengineering.stockexchange.feeds.ws.domain;

import java.io.IOException;
import java.io.Writer;
import org.codehaus.jackson.JsonGenerator;

/**
 *
 * @author imyousuf
 */
public interface JsonProvider {

  public String toJSON()
      throws IOException;

  public void toJSON(final Writer writer,
                     final JsonGenerator parentGenerator)
      throws IOException;
}
