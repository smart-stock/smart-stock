package com.smartitengineering.stockexchange.feeds.ws.domain;

import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import com.sun.syndication.feed.atom.Content;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.rss.Description;
import com.sun.syndication.feed.rss.Item;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.ws.rs.core.UriInfo;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

/**
 *
 * @author imyousuf
 */
public class QuotesResource
    implements JsonProvider {

  public static final String PROP_EXCHANGES = "exchanges";
  private final Map<String, StockExchange> exchanges;

  public QuotesResource(
      List<StockQuote> quotes) {
    this.exchanges = new TreeMap<String, StockExchange>();
    for (StockQuote quote : quotes) {

      final StockExchange actExchange;
      if (this.exchanges.get(quote.getCompany().toCompany().getExchange().
          getSymbolicName()) ==
          null) {
        StockExchange exchange = new StockExchange(
            quote.getCompany().toCompany().getExchange());
        this.exchanges.put(exchange.getSymbolicName(), exchange);
        actExchange = exchange;
      }
      else {
        actExchange = this.exchanges.get(quote.getCompany().toCompany().
            getExchange().getSymbolicName());
      }
      actExchange.addStockQuote(quote);
    }
  }

  public QuotesResource(
      Collection<com.smartitengineering.stockexchange.api.StockQuote> quotes) {
    com.smartitengineering.stockexchange.api.StockQuote[] oldQuotes;
    if (quotes == null || quotes.isEmpty()) {
      oldQuotes = new com.smartitengineering.stockexchange.api.StockQuote[0];
    }
    else {
      oldQuotes =
      quotes.toArray(
          new com.smartitengineering.stockexchange.api.StockQuote[quotes.size()]);
    }
    this.exchanges = new TreeMap<String, StockExchange>();
    for (com.smartitengineering.stockexchange.api.StockQuote quote : oldQuotes) {

      StockQuote actualQuote = new StockQuote(quote);
      final StockExchange actExchange;
      if (this.exchanges.get(quote.getCompany().getExchange().getSymbolicName()) ==
          null) {
        StockExchange exchange = new StockExchange(
            quote.getCompany().getExchange());
        this.exchanges.put(exchange.getSymbolicName(), exchange);
        actExchange = exchange;
      }
      else {
        actExchange = this.exchanges.get(quote.getCompany().getExchange().
            getSymbolicName());
      }
      actExchange.addStockQuote(actualQuote);
    }
  }

  public Map<String, StockExchange> getExchanges() {
    return exchanges;
  }

  public Channel toChannel(UriInfo uriInfo) {
    Channel channel = new Channel("rss_2.0");
    channel.setPubDate(new Date());
    channel.setLastBuildDate(new Date());
    channel.setLink(uriInfo.getRequestUri().toString());
    channel.setLanguage("en");
    List<Item> items = new ArrayList<Item>();
    channel.setItems(items);
    channel.setTitle("Stock Exchanes' Quotes");
    channel.setDescription("Quotes of different exchanges");
    for (StockExchange exchange : exchanges.values()) {
      for (StockQuote quote : exchange.getQuotes()) {
        Item item = new Item();
        items.add(item);
        item.setPubDate(quote.getQuoteTime());
        item.setTitle(new StringBuilder(exchange.getName()).append(" - ").
            append(quote.getCompany().getSymbol()).toString());
        Description summary = new Description();
        summary.setType("text/plain");
        summary.setValue(new StringBuilder("Last Price: ").append(quote.
            getLastPrice()).append(" High: ").append(quote.getHigh()).append(
            " Low: ").append(quote.getLow()).toString());
        item.setDescription(summary);
        final String companyWebUrl = SmartStockFactory.getLatestQuoteService().
            getCompanyWebUrl(quote.getCompany().toCompany());
        item.setLink(companyWebUrl);
        item.setLink(companyWebUrl);
      }
    }
    return channel;
  }

  public void toJSON(final Writer writer,
                     final JsonGenerator parentGenerator)
      throws IOException {
    JsonFactory factory = new JsonFactory();
    final JsonGenerator generator;
    if (parentGenerator == null) {
      generator = factory.createJsonGenerator(writer);
    }
    else {
      generator = parentGenerator;
    }
    generator.writeStartObject();
    generator.writeObjectFieldStart(PROP_EXCHANGES);
    for (Map.Entry<String, StockExchange> exchange : exchanges.entrySet()) {
      generator.writeObjectFieldStart(exchange.getKey());
      final String exchangeToJSON = exchange.getValue().toJSON();
      generator.writeRaw(
          exchangeToJSON.substring(1, exchangeToJSON.length() - 1));
      generator.writeEndObject();
    }
    generator.writeEndObject();
    generator.writeEndObject();
    if (parentGenerator == null) {
      generator.flush();
      generator.close();
    }
  }

  public String toJSON()
      throws IOException {
    StringWriter writer = new StringWriter();
    toJSON(writer, null);
    return writer.toString();
  }

  public Feed toFeed(UriInfo uriInfo) {
    Feed feed = new Feed("atom_0.3");
    feed.setId("latestQuotes");
    feed.setModified(new Date());
    List<Entry> entries = new ArrayList<Entry>();
    feed.setEntries(entries);
    feed.setTitle("Stock Exchanes' Quotes");
    for (StockExchange exchange : exchanges.values()) {
      for (StockQuote quote : exchange.getQuotes()) {
        Entry entry = new Entry();
        entries.add(entry);
        entry.setModified(quote.getQuoteTime());
        entry.setTitle(new StringBuilder(exchange.getName()).append(" - ").
            append(quote.getCompany().getSymbol()).toString());
        Content summary = new Content();
        summary.setType("text/plain");
        summary.setValue(new StringBuilder("Last Price: ").append(quote.
            getLastPrice()).append(" High: ").append(quote.getHigh()).append(
            " Low: ").append(quote.getLow()).toString());
        entry.setSummary(summary);
        entry.setPublished(quote.getQuoteTime());
      }
    }
    return feed;
  }
}
