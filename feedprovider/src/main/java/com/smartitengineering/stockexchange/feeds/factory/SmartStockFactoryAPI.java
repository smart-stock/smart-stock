package com.smartitengineering.stockexchange.feeds.factory;

import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.api.QuotesService;

/**
 *
 * @author imyousuf
 */
public interface SmartStockFactoryAPI {

  public QuotesService getQuotesService();

  public LatestQuoteService getLatestQuoteService();
}
