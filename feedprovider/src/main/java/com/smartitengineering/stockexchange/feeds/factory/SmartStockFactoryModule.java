package com.smartitengineering.stockexchange.feeds.factory;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import com.smartitengineering.stockexchange.api.DataConverter;
import com.smartitengineering.stockexchange.api.EntityWriteListener;
import com.smartitengineering.stockexchange.api.ExternalDataService;
import com.smartitengineering.stockexchange.api.KeyPredictor;
import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.impl.DataConverterImpl;
import com.smartitengineering.stockexchange.api.impl.KeyPredictorImpl;
import com.smartitengineering.stockexchange.api.impl.LatestQuoteServiceImpl;
import com.smartitengineering.stockexchange.api.impl.LatestQuotesServiceCacheImpl;
import com.smartitengineering.stockexchange.api.impl.QuotesServiceImpl;
import com.smartitengineering.stockexchange.api.impl.QuotesServiceCacheImpl;
import com.smartitengineering.stockexchange.dse.api.impl.DSEExternalDataServiceImpl;
import java.util.Collections;

/**
 *
 * @author imyousuf
 */
class SmartStockFactoryModule
    extends AbstractModule {

  @Override
  protected void configure() {
    bind(QuotesService.class).to(QuotesServiceCacheImpl.class).in(
        Scopes.SINGLETON);
    bind(LatestQuoteService.class).to(LatestQuotesServiceCacheImpl.class).in(
        Scopes.SINGLETON);
    bind(QuotesService.class).annotatedWith(Names.named("primary")).to(
        QuotesServiceImpl.class);
    bind(LatestQuoteService.class).annotatedWith(Names.named("primary")).to(
        LatestQuoteServiceImpl.class);
    bind(Integer.class).annotatedWith(Names.named("default")).toInstance(
        new Integer(2 * 60 * 1000));
    bind(Integer.class).annotatedWith(Names.named("long")).toInstance(
        new Integer(10 * 60 * 1000));
    bind(DataConverter.class).to(DataConverterImpl.class).in(Scopes.SINGLETON);
    bind(KeyPredictor.class).to(KeyPredictorImpl.class).in(Scopes.SINGLETON);
    bind(MemcacheService.class).toInstance(getMemcacheServiceInstance());
    Multibinder<ExternalDataService> uriBinder = Multibinder.newSetBinder(
        binder(), ExternalDataService.class);
    uriBinder.addBinding().to(DSEExternalDataServiceImpl.class);
    Multibinder<EntityWriteListener> listenerBinder = Multibinder.newSetBinder(
        binder(), EntityWriteListener.class);
    listenerBinder.addBinding().to(LatestQuotesServiceCacheImpl.class).in(
        Scopes.SINGLETON);
  }

  protected MemcacheService getMemcacheServiceInstance() {
    return MemcacheServiceFactory.getMemcacheService();
  }

  public static class SmartStockFactory
      implements SmartStockFactoryAPI {

    private static QuotesService quotesService;
    private static KeyPredictor predictor = new KeyPredictorImpl();
    private static DataConverter converter = new DataConverterImpl(predictor);
    private static LatestQuoteService latestQuoteService;

    public QuotesService getQuotesService() {
      if (quotesService == null) {
        initQuotesService();
      }
      return quotesService;
    }

    private synchronized void initQuotesService() {
      if (quotesService == null) {
        quotesService =
        new QuotesServiceCacheImpl(new QuotesServiceImpl(
            converter, predictor), new Integer(2 * 60 * 1000), new Integer(10 *
                                                                           60 *
                                                                           1000),
            MemcacheServiceFactory.getMemcacheService());
      }
    }

    public LatestQuoteService getLatestQuoteService() {
      if (latestQuoteService == null) {
        initLatestQuoteService();
      }
      return latestQuoteService;
    }

    private synchronized void initLatestQuoteService() {
      if (latestQuoteService == null) {
        LatestQuotesServiceCacheImpl cacheImpl = new LatestQuotesServiceCacheImpl(
            new Integer(2 * 60 * 1000),
            new Integer(120 * 60 * 1000), MemcacheServiceFactory.
            getMemcacheService());
        LatestQuoteServiceImpl quoteServiceImpl = new LatestQuoteServiceImpl(
            converter, predictor, getQuotesService(),
            Collections.<ExternalDataService>singleton(
            new DSEExternalDataServiceImpl()), Collections.<EntityWriteListener>
            singleton(cacheImpl));
        cacheImpl.setLatestQuoteService(quoteServiceImpl);
        latestQuoteService = cacheImpl;
      }
    }
  }
}
