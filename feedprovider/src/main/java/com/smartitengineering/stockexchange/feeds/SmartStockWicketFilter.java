package com.smartitengineering.stockexchange.feeds;

import com.google.inject.Singleton;
import org.apache.wicket.protocol.http.WicketFilter;

/**
 *
 * @author imyousuf
 */
@Singleton
public class SmartStockWicketFilter
    extends WicketFilter {
}
