function sortQuotes(compareFunction){
  var arrayOfQuotes = findShowedRowsInArray();
  arrayOfQuotes.sort(compareFunction);
  showAllAsIs(arrayOfQuotes);
}

function sortFunction(a,b,child,order){
//  if(a.is(".bookmarked") && !b.is(".bookmarked")) {
//    return -1;
//  }
//  if(!a.is(".bookmarked") && b.is(".bookmarked")) {
//    return 1;
//  }
  if(isNaN(a.children(child).text()) || isNaN(b.children(child).text())){
    if(a.children(child).text() > b.children(child).text()){
      return (1*order);
    }else
      return (-1*order);
  }
  else{
    if(parseFloat(a.children(child).text()) > parseFloat(b.children(child).text())){
      return (1*order);
    }else
      return (-1*order);
  }
}

function sortByTickrAsc(a,b){
  return sortFunction(a, b, ".companyLink", 1);
}
function sortByLastPriceAsc(a,b){
  return sortFunction(a, b, ".lastPrice", 1);
}
function sortByPriceChangeAsc(a,b){
  return sortFunction(a, b, ".changePercentage", 1);
}
function sortByHighestPriceAsc(a,b){
  return sortFunction(a, b, ".high", 1);
}
function sortByLowestPriceAsc(a,b){
  return sortFunction(a, b, ".low", 1);
}
function sortByTradeAsc(a,b){
  return sortFunction(a, b, ".trade", 1);
}
function sortByVolumeAsc(a,b){
  return sortFunction(a, b, ".volume", 1);
}
function sortByQuoteTimeAsc(a,b){
  return sortFunction(a, b, ".quoteTimeNumber", 1);
}

function sortByTickrDesc(a,b){
  return sortFunction(a, b, ".companyLink", -1);
}
function sortByLastPriceDesc(a,b){
  return sortFunction(a, b, ".lastPrice", -1);
}
function sortByPriceChangeDesc(a,b){
  return sortFunction(a, b, ".changePercentage", -1);
}
function sortByHighestPriceDesc(a,b){
  return sortFunction(a, b, ".high", -1);
}
function sortByLowestPriceDesc(a,b){
  return sortFunction(a, b, ".low", -1);
}
function sortByTradeDesc(a,b){
  return sortFunction(a, b, ".trade", -1);
}
function sortByVolumeDesc(a,b){
  return sortFunction(a, b, ".volume", -1);
}
function sortByQuoteTimeDesc(a,b){
  return sortFunction(a, b, ".quoteTimeNumber", -1);
}
