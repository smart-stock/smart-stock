function loadstocks(){
  var uri = finduri();  
  jQuery.ajax({
    type: "GET",
    url: uri,
    dataType: "json",
    timeout: 120000,
    success: function(data){
      $(".loading").text("Loading...");
      if(data==null){
        $(".loading").text("Data can't be loaded due to some error");
      }
      else
      {
        if(isLoadingFirstTime()){
          var arrayOfQuotes = new Array();
          var index=0;
          var baseuri = "http://www.stockbangladesh.com/company_details.php?code=";
          $.each(data.exchanges, function(i, se){
            $.each(se.quotes, function(j, x){
              arrayOfQuotes[index] = $(".savedExchange").clone(true);              
              arrayOfQuotes[index].addClass("exchange");
              arrayOfQuotes[index].removeClass("savedExchange");
              arrayOfQuotes[index].children(".companyLink").children(".exchangeHyperlink").attr("href", baseuri+x.company.symbol);
              arrayOfQuotes[index].children(".companyLink").children(".exchangeHyperlink").children(".companyName").text(x.company.symbol);
              arrayOfQuotes[index].children(".value").text(x.value);
              arrayOfQuotes[index].children(".lastPrice").text(x.lastPrice);
              arrayOfQuotes[index].children(".high").text(x.high);
              arrayOfQuotes[index].children(".low").text(x.low);
              arrayOfQuotes[index].children(".changePercentage").text(x.changePercentage);
              arrayOfQuotes[index].children(".trade").text(x.trade);
              arrayOfQuotes[index].children(".volume").text(x.volume);
              arrayOfQuotes[index].children(".version").text(x.version);
              var d = new Date(x.quoteTime);
              arrayOfQuotes[index].children(".quoteTime").text(getDateString(d));
              arrayOfQuotes[index].children(".quoteTimeNumber").text(x.quoteTime);
              arrayOfQuotes[index].attr("id", x.company.symbol);
              index++;
            });
          });
          var i;          
          for (i=0;i<index;i++){
            arrayOfQuotes[i].show();
            arrayOfQuotes[i].appendTo("#exchanges");
          }
          $(".savedExchange").remove();
          setRowColored();
          $(".loading").text("");
        }
        else{          
          $.each(data.exchanges, function(i, se){
            $.each(se.quotes, function(j, x){              
              $("#"+x.company.symbol).children(".value").text(x.value);
              $("#"+x.company.symbol).children(".lastPrice").text(x.lastPrice);
              $("#"+x.company.symbol).children(".high").text(x.high);
              $("#"+x.company.symbol).children(".low").text(x.low);
              $("#"+x.company.symbol).children(".changePercentage").text(x.changePercentage);
              $("#"+x.company.symbol).children(".trade").text(x.trade);
              $("#"+x.company.symbol).children(".volume").text(x.volume);
              $("#"+x.company.symbol).children(".version").text(x.version);
              var d = new Date(x.quoteTime);
              $("#"+x.company.symbol).children(".quoteTime").text(getDateString(d));
              $("#"+x.company.symbol).children(".quoteTimeNumber").text(x.quoteTime);              
            });
          });
          $(".loading").text("");
        }
      }
    },
    error: function(error){
      $(".loading").text("Data can't be loaded due to some error");
    }
  });
}

function finduri(){
  var symbol = $(".symbol").text();
  var fulluri = "/api/latest/" + symbol + ".json";
  var uri = "/api/exchange/" + symbol + ".json?";
  var total = 0;
  var numberVisible = 0;
  var isFirst = true;
  if(isLoadingFirstTime())
    return fulluri;
  $(".companyName").each(function(){
    if($(this).parents(".exchange").is(":visible")){
      if(isFirst){
        isFirst = false;
      }else{
        uri = uri + "&";
      }
      uri = uri + "tickr=" + $(this).text();
      numberVisible++;
    }
    total++;
  });
  if(numberVisible > total/2 || numberVisible == 0){
    return fulluri;
  }else{
    return uri;
  }
}

function isLoadingFirstTime(){
  var totalExchange=0;
  $(".exchange").each(function(){
    totalExchange++;
    if(totalExchange>0)
      return false;
  });
  if(totalExchange==0)
    return true;
  else
    return false;
  

}

