function setRowColored(){
  var index=0;
  $(".companyName").each(function(){
    if($(this).parents(".exchange").is(":visible")){
      $(this).parents(".exchange").removeClass("even");
      $(this).parents(".exchange").removeClass("odd");
      $(this).parents(".exchange").addClass(index % 2 == 0 ? "even" : "odd");
      index++;
    }
  });
}

function findShowedRowsInArray(){
  var arrayOfQuotes = new Array();
  var index = 0;
  $(".bookmarked").each(function(){
    arrayOfQuotes[index]=$(this).clone(true);
    index++;
  });
  $(".nbookmarked").each(function(){
    arrayOfQuotes[index]=$(this).clone(true);
    index++;
  });
  return arrayOfQuotes;
}

function showSearchedResult(){
  $(".searchResultTab").removeClass("unselectedTab");
  $(".searchResultTab").addClass("selectedTab");
  $(".bookmarkTab").removeClass("selectedTab");
  $(".bookmarkTab").addClass("unselectedTab");
  $(".companyName").each(function() {
    if($(this).text().toLowerCase().indexOf($("#searchText").val().toLowerCase(), 0) < 0) {
      $(this).parents(".exchange").hide();
    }
    else {
      $(this).parents(".exchange").show();
    }
  });
  setRowColored();
}

function showAllWithBookmarkedAtTop(array){
  var arrayOfQuotes;
  if(array==null){
    arrayOfQuotes = findShowedRowsInArray();
  }else{
    arrayOfQuotes = array;
  }
  var orderedNodes = arrayOfQuotes;
  var i = 0;
  $(".exchange").each(function(){
    if(orderedNodes[i].is(".bookmarked")){
      orderedNodes[i].show();
    }
    $(this).replaceWith(orderedNodes[i]);
    i++;
  });
  setRowColored();
}

function showAllAsIs(array){
  var arrayOfQuotes;
  if(array==null){
    arrayOfQuotes = findShowedRowsInArray();
  }else{
    arrayOfQuotes = array;
  }
  var i = 0;
  $(".exchange").each(function(){
    $(this).replaceWith(arrayOfQuotes[i]);
    i++;
  });
  setRowColored();
}

function getDateString(date){
  var d = "";
  if(date.getHours()<10){
    d = d + "0";
  }
  d = d + date.getHours() + ":";

  if(date.getMinutes()<10){
    d = d + "0";
  }
  d = d + date.getMinutes() + ":";
  if(date.getSeconds()<10){
    d = d + "0";
  }
  d = d + date.getSeconds();

  d = d + ",";

  if(date.getDate()<10){
    d = d + "0";
  }
  d = d + date.getDate() + "-";
  if((date.getMonth() + 1)<10){
    d = d + "0";
  }
  d = d + (date.getMonth() + 1) + "-";

  
  d = d + date.getFullYear();

  return d;
}
