package com.smartitengineering.stockexchange.feeds.factory;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.impl.QuotesServiceCacheImpl;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.jmock.Mockery;
import org.jmock.integration.junit3.JUnit3Mockery;

/**
 *
 * @author imyousuf
 */
public class GuiceModuleTest
    extends TestCase {

  private Mockery context = new JUnit3Mockery();

  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public GuiceModuleTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(GuiceModuleTest.class);
  }

  /**
   * Rigourous Test :-)
   */
  public void testModule() {
    Injector injector = Guice.createInjector(new SmartStockFactoryModule(){

      @Override
      protected MemcacheService getMemcacheServiceInstance() {
        return context.mock(MemcacheService.class);
      }

    });
    final QuotesService quotesService =
        injector.getInstance(QuotesService.class);
    assertNotNull(quotesService);
    assertEquals(quotesService.getClass(), QuotesServiceCacheImpl.class);
  }
}
