package com.smartitengineering.stockexchange.api.impl;

import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
@Singleton
public class QuotesServiceCacheImpl
    extends AbstractCacheImpl
    implements QuotesService {

  private final QuotesService quotesService;
  private final MemcacheService memcacheService;
  private final int defaultExprirationDurationInMillis;
  private final int longExprirationDurationInMillis;
  private final static Logger log = Logger.getLogger(
      QuotesServiceCacheImpl.class.getName());

  @Inject
  public QuotesServiceCacheImpl(@Named("primary") QuotesService quotesService,
                                @Named("default") Integer defaultExprirationDurationInMillis,
                                @Named("long") Integer longExprirationDurationInMillis,
                                MemcacheService memcacheService) {
    this.memcacheService = memcacheService;
    this.quotesService = quotesService;
    this.defaultExprirationDurationInMillis =
    defaultExprirationDurationInMillis.intValue();
    this.longExprirationDurationInMillis = longExprirationDurationInMillis.
        intValue();
  }

  public Collection<StockExchange> getStockExchanges() {
    log.log(Level.FINE, "getStockExcahanges() in Cache Layer");
    Object cachedValue = memcacheService.get(ALL_EXCHANGES);
    if (cachedValue instanceof Collection) {
      log.log(Level.FINE, "Cache HIT!");
      return (Collection<StockExchange>) cachedValue;
    }
    log.log(Level.FINE, "Cache MISS!");
    final Collection<StockExchange> exchanges =
                                    quotesService.getStockExchanges();
    if (exchanges != null && !exchanges.isEmpty()) {
      memcacheService.put(ALL_EXCHANGES, exchanges, Expiration.onDate(
          APIImplUtil.getNextExpirationDate()));
    }
    return exchanges;
  }

  public StockExchange getStockExchange(String symbolicName) {
    if (StringUtils.isNotBlank(symbolicName)) {
      Object cachedValue = memcacheService.get(symbolicName);
      if (cachedValue instanceof StockExchange) {
        return (StockExchange) cachedValue;
      }
    }
    final StockExchange exchange = quotesService.getStockExchange(symbolicName);
    if (StringUtils.isNotBlank(symbolicName) && exchange != null) {
      memcacheService.put(symbolicName, exchange, Expiration.onDate(APIImplUtil.
          getNextExpirationDate()));
    }
    return exchange;
  }

  public Company getCompany(String companyTickr,
                            StockExchange exchange) {
    if (StringUtils.isNotBlank(companyTickr) && exchange != null) {
      Object cachedValue = memcacheService.get(getCacheKey(exchange,
          companyTickr));
      if (cachedValue instanceof Company) {
        return (Company) cachedValue;
      }
    }
    final Company company = quotesService.getCompany(companyTickr, exchange);
    if (StringUtils.isNotBlank(companyTickr) && exchange != null && company !=
                                                                    null) {
      memcacheService.put(getCacheKey(exchange, company), company, Expiration.
          onDate(APIImplUtil.getNextExpirationDate()));
    }
    return company;
  }

  public Map<Company, StockQuote> getQuotesMapForStockExchangeFromPersistentStorage(
      StockExchange exchange) {
    if (exchange != null && StringUtils.isNotBlank(exchange.getSymbolicName())) {
      Object cachedValue = memcacheService.get(exchange);
      if (cachedValue instanceof Map) {
        return (Map<Company, StockQuote>) cachedValue;
      }
    }
    final Map<Company, StockQuote> quotes =
                                   quotesService.
        getQuotesMapForStockExchangeFromPersistentStorage(exchange);
    if (exchange != null && StringUtils.isNotBlank(exchange.getSymbolicName()) &&
        quotes != null) {
      memcacheService.put(exchange, quotes, Expiration.onDate(APIImplUtil.
          getNextExpirationDate()));
    }
    return quotes;
  }

  public Collection<StockQuote> getQuotesForCompanyFromPersistentStorage(
      Company company) {
    if (company != null && StringUtils.isNotBlank(company.getSymbol())) {
      Object cachedValue = memcacheService.get(company);
      if (cachedValue instanceof Collection) {
        return (Collection<StockQuote>) cachedValue;
      }
    }
    final Collection<StockQuote> quotes =
                                 quotesService.
        getQuotesForCompanyFromPersistentStorage(company);
    if (company != null && StringUtils.isNotBlank(company.getSymbol()) &&
        quotes != null) {
      memcacheService.put(company, quotes, Expiration.onDate(APIImplUtil.
          getNextExpirationDate()));
    }
    return quotes;
  }

  public Collection<StockQuote> getQuotesForStockExchangeFromPersistentStorage(
      StockExchange exchange) {
    final Map<Company, StockQuote> stockQuotesMap =
                                   getQuotesMapForStockExchangeFromPersistentStorage(
        exchange);
    return stockQuotesMap == null ? null : Collections.unmodifiableCollection(stockQuotesMap.
        values());
  }
}
