package com.smartitengineering.stockexchange.api.impl;

import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.EntityWriteListener;
import com.smartitengineering.stockexchange.api.Event;
import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
@Singleton
public class LatestQuotesServiceCacheImpl
    extends AbstractCacheImpl
    implements LatestQuoteService,
               EntityWriteListener {

  private LatestQuoteService latestQuoteService;
  private final MemcacheService memcacheService;
  private final int defaultExprirationDurationInMillis;
  private final int longExprirationDurationInMillis;
  private final static Logger log = Logger.getLogger(
      LatestQuotesServiceCacheImpl.class.getName());

  @Inject
  public LatestQuotesServiceCacheImpl(
      @Named("default") Integer defaultExprirationDurationInMillis,
      @Named("long") Integer longExprirationDurationInMillis,
      MemcacheService memcacheService) {
    this.memcacheService = memcacheService;
    this.defaultExprirationDurationInMillis =
    defaultExprirationDurationInMillis.intValue();
    this.longExprirationDurationInMillis = longExprirationDurationInMillis.
        intValue();
  }

  @Inject
  public void setLatestQuoteService(
      @Named("primary") LatestQuoteService latestQuoteService) {
    this.latestQuoteService = latestQuoteService;
  }

  public Map<Company, StockQuote> getLatestStockQuotesMapFromExternalSource(
      StockExchange exchange,
      boolean persist) {
    Map<Company, StockQuote> quotes = latestQuoteService.
        getLatestStockQuotesMapFromExternalSource(exchange, persist);
    if (exchange != null && StringUtils.isNotBlank(exchange.getSymbolicName()) &&
        persist && quotes != null) {
      deleteFromCacheForExchange(exchange, quotes.values());
      memcacheService.put(exchange, quotes, Expiration.onDate(APIImplUtil.
          getNextExpirationDate()));
    }
    log.finer("Return from map latest quotes service cache layer!");
    return quotes;
  }

  public Collection<StockQuote> getLatestStockQuotesFromExternalSource(
      StockExchange exchange,
      boolean persist) {
    final Map<Company, StockQuote> latestStockQuotesMapFromExternalSource =
                                   getLatestStockQuotesMapFromExternalSource(
        exchange, persist);
    log.finer("Return from collection latest quotes service cache layer!");
    return latestStockQuotesMapFromExternalSource == null ? null : Collections.
        unmodifiableCollection(
        latestStockQuotesMapFromExternalSource.values());
  }

  public String getCompanyWebUrl(Company company) {
    final boolean validCompany = checkValidCompany(company);
    if (validCompany) {
      Object cachedValue = memcacheService.get(getCompanyURLCacheKey(company));
      if (cachedValue instanceof String) {
        return (String) cachedValue;
      }
    }
    final String companyWebUrl = latestQuoteService.getCompanyWebUrl(company);
    if (validCompany && StringUtils.isNotBlank(companyWebUrl)) {
      memcacheService.put(getCompanyURLCacheKey(company), companyWebUrl,
          Expiration.onDate(APIImplUtil.getNextExpirationDate()));
    }
    return companyWebUrl;
  }

  private boolean checkValidCompany(Company company) {
    return company != null && StringUtils.isNotBlank(company.getSymbol()) &&
           company.getExchange() != null && StringUtils.isNotBlank(company.
        getExchange().getSymbolicName());
  }

  private void deleteFromCacheForExchange(StockExchange exchange,
                                          Collection<StockQuote> quotes) {
    final String jsonKey = new StringBuilder(QUOTES_JSON_PREFIX).append(
        exchange.getSymbolicName()).toString();
    memcacheService.delete(jsonKey);
    final String atomKey = new StringBuilder(QUOTES_ATOM_PREFIX).append(
        exchange.getSymbolicName()).toString();
    memcacheService.delete(atomKey);
    if (log.isLoggable(Level.FINEST)) {
      log.log(Level.FINEST, new StringBuilder("JSON Key class: ").append(
          jsonKey.getClass()).append(" Key: ").append(jsonKey).toString());
      log.log(Level.FINEST, new StringBuilder("ATOM Key class: ").append(
          atomKey.getClass()).append(" Key: ").append(atomKey).toString());
    }
  }

  public boolean addEventWriteListener(EntityWriteListener listener) {
    return latestQuoteService.addEventWriteListener(listener);
  }

  public boolean removeEventWriteListener(EntityWriteListener listener) {
    return latestQuoteService.removeEventWriteListener(listener);
  }

  public Collection<EntityWriteListener> getRegisteredListeners() {
    return latestQuoteService.getRegisteredListeners();
  }

  public void entityWritten(Event... events) {
    if (events == null || events.length <= 0) {
      return;
    }
    Collection<Event> exchangeEvents = new HashSet<Event>();
    Collection<Event> companyEvents = new HashSet<Event>();
    Collection<Event> quoteEvents = new HashSet<Event>();
    for (Event event : events) {
      switch (event.getEntityType()) {
        case COMPANY:
          companyEvents.add(event);
          break;
        case STOCK_EXCHANGE:
          exchangeEvents.add(event);
          break;
        case STOCK_QUOTE:
          quoteEvents.add(event);
          break;
        default:
      }
    }
    stockExchangeEventsWritten(exchangeEvents);
    companyEventsWritten(companyEvents);
    stockQuoteEventsWritten(quoteEvents);
  }

  protected void stockExchangeEventsWritten(Collection<Event> exchangeEvents) {
    if (exchangeEvents.isEmpty()) {
      return;
    }
    memcacheService.delete(ALL_EXCHANGES);
    for (Event<StockExchange> event : exchangeEvents) {
      switch (event.getAction()) {
        case UPDATE:
        case DELETE:
          memcacheService.delete(event.getObject().getSymbolicName());
          memcacheService.delete(event.getOldObject());
          break;
        case CREATE:
        default:
      }
    }
  }

  protected void companyEventsWritten(Collection<Event> companyEvents) {
    if (companyEvents.isEmpty()) {
      return;
    }
    for (Event<Company> event : companyEvents) {
      switch (event.getAction()) {
        case UPDATE:
        case DELETE:
          memcacheService.delete(getCacheKey(event.getObject().getExchange(),
              event.getObject()));
          memcacheService.delete(event.getOldObject());
          memcacheService.delete(getCompanyURLCacheKey(event.getOldObject()));
          break;
        case CREATE:
        default:
      }
    }
  }

  protected void stockQuoteEventsWritten(Collection<Event> quoteEvents) {
    if (quoteEvents.isEmpty()) {
      return;
    }
    for (Event<StockQuote> event : quoteEvents) {
      final StockQuote object = event.getObject();
      final Company company;
      if (object != null) {
        company = object.getCompany();
      }
      else {
        company = null;
      }
      switch (event.getAction()) {
        case UPDATE:
        case DELETE:
          if (company != null && memcacheService.contains(company)) {
            memcacheService.delete(company);
          }
          break;
        case CREATE:
        default:
      }
    }
  }
}
