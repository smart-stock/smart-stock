package com.smartitengineering.stockexchange.api.impl;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.DataConverter;
import com.smartitengineering.stockexchange.api.EntityWriteListener;
import com.smartitengineering.stockexchange.api.Event;
import com.smartitengineering.stockexchange.api.ExternalDataService;
import com.smartitengineering.stockexchange.api.KeyPredictor;
import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
@Singleton
public class LatestQuoteServiceImpl
    implements LatestQuoteService {

  private final Map<String, ExternalDataService> externalDataServices;
  private final DataConverter dataConverter;
  private final DatastoreService service;
  private final KeyPredictor predictor;
  private final QuotesService quotesService;
  private static final Logger log = Logger.getLogger(
      LatestQuoteServiceImpl.class.getName());
  private final Set<EntityWriteListener> registeredListeners;

  @Inject
  public LatestQuoteServiceImpl(DataConverter dataConverter,
                                KeyPredictor predictor,
                                QuotesService quotesService,
                                Set<ExternalDataService> externalDataServices,
                                Set<EntityWriteListener> listeners) {
    this.service = DatastoreServiceFactory.getDatastoreService();
    this.dataConverter = dataConverter;
    this.predictor = predictor;
    this.quotesService = quotesService;
    this.externalDataServices = new HashMap<String, ExternalDataService>();
    if (externalDataServices != null && externalDataServices.size() > 0) {
      for (ExternalDataService externalDataService : externalDataServices) {
        this.externalDataServices.put(
            externalDataService.getStockExchangeSymbol(),
            externalDataService);
      }
    }
    if (log.isLoggable(Level.INFO)) {
      log.log(Level.INFO, new StringBuilder("Exchanges for ").append(
          externalDataServices).append(" external data services").toString());
    }
    registeredListeners = new HashSet<EntityWriteListener>();
    registeredListeners.addAll(listeners);
    createStockExchangesIfNeeded(externalDataServices.toArray(
        new ExternalDataService[externalDataServices.size()]));
  }

  protected void createStockExchangesIfNeeded(
      final ExternalDataService[] externalDataServices) {
    if (log.isLoggable(Level.INFO)) {
      log.log(Level.INFO, new StringBuilder("Creating exchanges for ").append(
          externalDataServices).append(" external data services").toString());
    }
    if (externalDataServices != null && externalDataServices.length > 0) {
      for (ExternalDataService eds : externalDataServices) {
        if (log.isLoggable(Level.INFO)) {
          log.log(Level.INFO, new StringBuilder("Trying ").append(eds.
              getStockExchangeSymbol()).append(" exchange").toString());
        }
        if (quotesService.getStockExchange(eds.getStockExchangeSymbol()) == null) {
          try {
            if (log.isLoggable(Level.INFO)) {
              log.log(Level.INFO, new StringBuilder("Creating ").append(eds.
                  getStockExchangeSymbol()).append(" exchange").toString());
            }
            Transaction transaction = service.beginTransaction();
            final StockExchange stockExchange = eds.getRawStockExchange();
            final Entity stockExchangeEntity =
                         dataConverter.convertStockExchangeToEntity(
                stockExchange);
            service.put(transaction, stockExchangeEntity);
            transaction.commit();
            EventImpl event = new EventImpl();
            event.setAction(Event.Action.CREATE);
            event.setEntityType(Event.EntityType.STOCK_EXCHANGE);
            event.setObject(stockExchange);
            event.setObjectKey(stockExchangeEntity.getKey());
            fireEvents(event);
            if (log.isLoggable(Level.INFO)) {
              log.log(Level.INFO, new StringBuilder("Created ").append(eds.
                  getStockExchangeSymbol()).append(" exchange").toString());
            }
            stockExchange.setId(stockExchangeEntity.getKey().getId());
          }
          catch (Exception ex) {
            if (service.getCurrentTransaction() != null && service.
                getCurrentTransaction().isActive()) {
              service.getCurrentTransaction().rollback();
            }
            if (log.isLoggable(Level.WARNING)) {
              log.log(Level.WARNING, new StringBuilder(
                  "Error creating exchange - ").append(eds.
                  getStockExchangeSymbol()).append(". Cause: ").append(ex.
                  getMessage()).append("For details turn on FINER Level!").
                  toString());
            }
            log.log(Level.FINER, ex.getMessage(), ex);
            break;
          }
        }
      }
    }
  }

  public Map<Company, StockQuote> getLatestStockQuotesMapFromExternalSource(
      StockExchange exchange,
      boolean persist) {
    if (exchange != null) {
      ExternalDataService externalDataService = externalDataServices.get(
          exchange.getSymbolicName());
      if (externalDataService != null) {
        Map<Company, StockQuote> currentQuotes =
                                 new HashMap<Company, StockQuote>(quotesService.
            getQuotesMapForStockExchangeFromPersistentStorage(exchange));
        Collection<StockQuote> quotes =
                               externalDataService.getLatestLiveStockQuotes();
        Map<Company, StockQuote> diffMap = new HashMap<Company, StockQuote>(
            quotes.size());
        for (StockQuote quote : quotes) {
          if (quote == null) {
            continue;
          }
          StockQuote currentQuote = currentQuotes.get(quote.getCompany());
          if (!ObjectUtils.equals(quote, currentQuote)) {
            diffMap.put(quote.getCompany(), quote);
            currentQuotes.put(quote.getCompany(), quote);
          }
        }
        if (persist && quotes != null && !quotes.isEmpty()) {
          persistLatestQuotes(exchange, diffMap.values());
        }
        if (diffMap.isEmpty()) {
          return null;
        }
        else {
          log.finer("Return from map latest quotes service layer!");
          return Collections.unmodifiableMap(currentQuotes);
        }
      }
    }
    log.finer("Return from empty map latest quotes service layer!");
    return Collections.emptyMap();
  }

  public Collection<StockQuote> getLatestStockQuotesFromExternalSource(
      StockExchange exchange,
      boolean persist) {
    final Map<Company, StockQuote> latestStockQuotesMapFromExternalSource =
                                   getLatestStockQuotesMapFromExternalSource(
        exchange, persist);
    log.finer("Return from collection latest quotes service layer!");
    return latestStockQuotesMapFromExternalSource == null ? null : Collections.
        unmodifiableCollection(latestStockQuotesMapFromExternalSource.values());
  }

  protected void persistLatestQuotes(StockExchange exchange,
                                     Collection<StockQuote> quotes) {
    Transaction transaction = service.beginTransaction();
    boolean error = false;
    List<Entity> entities = new ArrayList<Entity>(quotes.size());
    List<Event> events = new ArrayList<Event>(quotes.size());
    try {
      for (StockQuote quote : quotes) {

        if (quotesService.getCompany(quote.getCompany().getSymbol(), exchange) ==
            null) {
          Entity companyEntity =
                 dataConverter.convertCompanyToEntity(quote.getCompany());
          entities.add(companyEntity);
          EventImpl event = new EventImpl();
          event.setAction(Event.Action.CREATE);
          event.setEntityType(Event.EntityType.COMPANY);
          event.setObject(quote.getCompany());
          event.setObjectKey(companyEntity.getKey());
          events.add(event);
        }
        Entity quoteEntity = dataConverter.convertStockQuoteToEntity(quote);
        entities.add(quoteEntity);
        final Key latestQuoteKey = predictor.getLatestStockKey(exchange, quote.
            getCompany());
        Entity latestQuoteEntity = new Entity(latestQuoteKey);
        latestQuoteEntity.setPropertiesFrom(quoteEntity);
        entities.add(latestQuoteEntity);
        EventImpl event = new EventImpl();
        event.setAction(Event.Action.UPDATE);
        event.setEntityType(Event.EntityType.STOCK_QUOTE);
        event.setObject(quote);
        event.setObjectKey(quoteEntity.getKey());
        events.add(event);
        event = new EventImpl();
        event.setAction(Event.Action.CREATE);
        event.setEntityType(Event.EntityType.STOCK_QUOTE);
        event.setObject(quote);
        event.setObjectKey(quoteEntity.getKey());
        events.add(event);
      }
      service.put(transaction, entities);
    }
    catch (Exception ex) {
      error = true;
      log.log(Level.WARNING, ex.getMessage(), ex);
    }
    if (error) {
      transaction.rollback();
    }
    else {
      transaction.commit();
      fireEvents(events.toArray(new Event[events.size()]));
    }
  }

  public String getCompanyWebUrl(Company company) {
    if (company.getExchange() != null && StringUtils.isNotBlank(company.
        getExchange().getSymbolicName()) && externalDataServices.containsKey(company.getExchange().
        getSymbolicName())) {
      return externalDataServices.get(company.getExchange().getSymbolicName()).
          getCompanyUrl(company);
    }
    return "";
  }

  protected void fireEvents(Event... events) {
    for (EntityWriteListener listener : registeredListeners) {
      listener.entityWritten(events);
    }
  }

  public boolean addEventWriteListener(EntityWriteListener listener) {
    if (listener != null) {
      return registeredListeners.add(listener);
    }
    return false;
  }

  public boolean removeEventWriteListener(EntityWriteListener listener) {
    if (listener != null) {
      return registeredListeners.remove(listener);
    }
    return false;
  }

  public Collection<EntityWriteListener> getRegisteredListeners() {
    return Collections.unmodifiableSet(registeredListeners);
  }
}
