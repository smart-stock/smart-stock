package com.smartitengineering.stockexchange.api.impl;

import com.google.appengine.api.datastore.Entity;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.DataConverter;
import com.smartitengineering.stockexchange.api.KeyPredictor;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imyousuf
 * @author modhu7
 */
@Singleton
public class DataConverterImpl
        implements DataConverter {

  private static final Logger log = Logger.getLogger(
          DataConverterImpl.class.getName());
  private final KeyPredictor predictor;

  @Inject
  public DataConverterImpl(KeyPredictor predictor) {
    this.predictor = predictor;
  }

  public Entity convertStockExchangeToEntity(StockExchange exchange) {
    final Entity entity = new Entity(predictor.getExchangeKey(exchange));
    entity.setUnindexedProperty(StockExchange.PROP_NAME, exchange.getName());
    entity.setUnindexedProperty(StockExchange.PROP_ADDRESS, exchange.getAddress());
    entity.setProperty(StockExchange.PROP_SYMBOLIC_NAME, exchange.
            getSymbolicName());
    entity.setUnindexedProperty(StockExchange.PROP_TRADING_END_TIME, exchange.
            getTradingEndTime());
    entity.setUnindexedProperty(StockExchange.PROP_TRADING_START_TIME, exchange.
            getTradingStartTime());
    entity.setUnindexedProperty(StockExchange.PROP_WEB_URL, exchange.getWebsiteUrl());
    return entity;
  }

  public Entity convertCompanyToEntity(Company company) {
    final Entity entity = new Entity(predictor.getCompanyKey(
            company.getExchange(), company));
    entity.setUnindexedProperty(Company.PROP_CAT, company.getCategory());
    entity.setUnindexedProperty(Company.PROP_DESC, company.getDescription());
    entity.setProperty(Company.PROP_EXCHANGE_SYMBOL, company.getExchange().
            getSymbolicName());
    entity.setUnindexedProperty(Company.PROP_FACE_VAL, company.getFaceValue());
    entity.setUnindexedProperty(Company.PROP_LOT_SIZE, company.getLotSize());
    entity.setUnindexedProperty(Company.PROP_SECT, company.getSector());
    entity.setProperty(Company.PROP_SYMBOL, company.getSymbol());
    return entity;
  }

  public Entity convertStockQuoteToEntity(StockQuote quote) {
    final Entity entity = new Entity(QuotesService.Kind.STOCK_QUOTES.name(),
            predictor.getCompanyKey(quote.getCompany().getExchange(), quote.
            getCompany()));
    entity.setUnindexedProperty(StockQuote.PROP_CHANGE_PERCENT,
            quote.getChangePercentage());
    entity.setProperty(StockQuote.PROP_COMPANY_TICKR, quote.getCompany().
            getSymbol());
    entity.setUnindexedProperty(StockQuote.PROP_HIGH, quote.getHigh());
    entity.setUnindexedProperty(StockQuote.PROP_LAST_PRICE, quote.getLastPrice());
    entity.setUnindexedProperty(StockQuote.PROP_LOW, quote.getLow());
    entity.setProperty(StockQuote.PROP_QUOTE_TIME, quote.getQuoteTime());
    entity.setProperty(StockQuote.PROP_STOCK_XCHANGE_SYBMOL, quote.getCompany().
            getExchange().getSymbolicName());
    entity.setUnindexedProperty(StockQuote.PROP_TRADE, quote.getTrade());
    entity.setUnindexedProperty(StockQuote.PROP_VALUE, quote.getValue());
    entity.setUnindexedProperty(StockQuote.PROP_VOLUME, quote.getVolume());
    return entity;
  }

  public Company convertEntityToCompany(Entity entity,
                                        StockExchange exchange) {
    final Company company = new Company();
    company.setId(entity.getKey().getId());
    company.setCategory((String) entity.getProperty(Company.PROP_CAT));
    company.setDescription((String) entity.getProperty(Company.PROP_DESC));
    company.setExchange(exchange);
    try {
      company.setFaceValue(Double.valueOf(entity.getProperty(
              Company.PROP_FACE_VAL).
              toString()));      
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in company conversion by property faceValue! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }
    try {
      company.setLotSize(Integer.valueOf(entity.getProperty(
              Company.PROP_LOT_SIZE).
              toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in company conversion by the property lotSize! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }
    company.setSector((String) entity.getProperty(Company.PROP_SECT));
    company.setSymbol((String) entity.getProperty(Company.PROP_SYMBOL));
    return company;
  }

  public StockExchange convertEntityToStockExchange(Entity entity) {
    final StockExchange exchange = new StockExchange();
    exchange.setName((String) entity.getProperty(StockExchange.PROP_NAME));
    exchange.setAddress((String) entity.getProperty(StockExchange.PROP_ADDRESS));
    exchange.setSymbolicName((String) entity.getProperty(
            StockExchange.PROP_SYMBOLIC_NAME));
    exchange.setTradingEndTime((String) entity.getProperty(
            StockExchange.PROP_TRADING_END_TIME));
    exchange.setTradingStartTime((String) entity.getProperty(
            StockExchange.PROP_TRADING_START_TIME));
    exchange.setWebsiteUrl((String) entity.getProperty(
            StockExchange.PROP_WEB_URL));
    exchange.setId(entity.getKey().getId());
    return exchange;
  }

  public StockQuote convertEntityToStockQuote(Entity entity,
                                              Company company) {
    final StockQuote quote = new StockQuote();
    quote.setId(entity.getKey().getId());
    try {
      quote.setChangePercentage(Double.valueOf(entity.getProperty(
              StockQuote.PROP_CHANGE_PERCENT).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property changePercentage! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }
    quote.setCompany(company);

    try {
      quote.setHigh(Double.valueOf(entity.getProperty(
              StockQuote.PROP_HIGH).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property high! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }

    try {
      quote.setLastPrice(Double.valueOf(entity.getProperty(
              StockQuote.PROP_LAST_PRICE).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property lastPrice! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }

    try {
      quote.setLow(Double.valueOf(entity.getProperty(
              StockQuote.PROP_LOW).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property low! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }

    try {
      quote.setValue(Double.valueOf(entity.getProperty(
              StockQuote.PROP_VALUE).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property value! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }

    try {
      quote.setTrade(Long.valueOf(entity.getProperty(
              StockQuote.PROP_TRADE).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property trade! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }

    try {
      quote.setVolume(Long.valueOf(entity.getProperty(
              StockQuote.PROP_VOLUME).toString()));
    }
    catch (Exception exception) {
      log.log(Level.WARNING,
              "Exception in StockQuote conversion by the property volume! Check FINER for details");
      log.log(Level.FINER, exception.getMessage(), exception);
    }
    quote.setQuoteTime((Date) entity.getProperty(StockQuote.PROP_QUOTE_TIME));
    return quote;
  }
}
