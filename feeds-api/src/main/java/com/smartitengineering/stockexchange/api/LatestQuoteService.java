package com.smartitengineering.stockexchange.api;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author imyousuf
 */
public interface LatestQuoteService {

  public static final String QUOTES_JSON_PREFIX = "QUOTES_JSON_";
  public static final String QUOTES_ATOM_PREFIX = "QUOTES_ATOM_";

  /**
   * Retrieve the latest quotes for speicifed stock exchange and persist them to
   * storage if speicified.
   * @param exchange Stock Exchange to retrieve the latest quotes for
   * @param persist Whether to persist the change or not
   * @return null if no change from last quotes else collection of quotes.
   */
  public Collection<StockQuote> getLatestStockQuotesFromExternalSource(
      StockExchange exchange,
      boolean persist);

  public Map<Company, StockQuote> getLatestStockQuotesMapFromExternalSource(
      StockExchange exchange,
      boolean persist);

  public String getCompanyWebUrl(Company company);

  public boolean addEventWriteListener(EntityWriteListener listener);

  public boolean removeEventWriteListener(EntityWriteListener listener);

  /**
   * Return all registered {@link EntityWriteListener listeners}
   * @return Immutable collection of listeners
   */
  public Collection<EntityWriteListener> getRegisteredListeners();
}
