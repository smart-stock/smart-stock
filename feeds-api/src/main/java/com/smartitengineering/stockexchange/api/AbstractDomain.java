package com.smartitengineering.stockexchange.api;

import java.io.Serializable;

/**
 *
 * @author imyousuf
 */
public abstract class AbstractDomain
    implements Serializable {

  private long id;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }
}
