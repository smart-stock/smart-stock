package com.smartitengineering.stockexchange.api;

/**
 *
 * @author imyousuf
 */
public class Company
        extends AbstractDomain {

  public static final String PROP_ID = "quoteId";
  public static final String PROP_SYMBOL = "symbol";
  public static final String PROP_SECT = "sector";
  public static final String PROP_CAT = "category";
  public static final String PROP_LOT_SIZE = "lotSize";
  public static final String PROP_FACE_VAL = "faceValue";
  public static final String PROP_DESC = "description";
  public static final String PROP_EXCHANGE_SYMBOL = "exchangeSymbol";
  private String symbol;
  private String sector;
  private String category;
  private int lotSize;
  private double faceValue;
  private String description;
  private StockExchange exchange;

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public double getFaceValue() {
    return faceValue;
  }

  public void setFaceValue(double faceValue) {
    this.faceValue = faceValue;
  }

  public int getLotSize() {
    return lotSize;
  }

  public void setLotSize(int lotSize) {
    this.lotSize = lotSize;
  }

  public String getSector() {
    return sector;
  }

  public void setSector(String sector) {
    this.sector = sector;
  }

  public String getSymbol() {
    return symbol == null ? "" : symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public StockExchange getExchange() {
    return exchange;
  }

  public void setExchange(StockExchange exchange) {
    this.exchange = exchange;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Company other = (Company) obj;
    if ((this.symbol == null) ? (other.symbol != null)
        : !this.symbol.equals(other.symbol)) {
      return false;
    }
    if (this.exchange != other.exchange &&
        (this.exchange == null || !this.exchange.equals(other.exchange))) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 83 * hash + (this.symbol != null ? this.symbol.hashCode() : 0);
    hash = 83 * hash + (this.exchange != null ? this.exchange.hashCode() : 0);
    return hash;
  }
}
