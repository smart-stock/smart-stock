package com.smartitengineering.stockexchange.api;

/**
 *
 * @author imyousuf
 */
public interface EntityWriteListener {

  public void entityWritten(Event... events);
}
