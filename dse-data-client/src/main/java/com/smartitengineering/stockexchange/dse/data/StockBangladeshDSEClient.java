package com.smartitengineering.stockexchange.dse.data;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.io.StringReader;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author imyousuf
 */
public class StockBangladeshDSEClient {

  private static final String FF_UA =
                              "Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.9.0.2) Gecko/20121223 Ubuntu/9.25 (jaunty) Firefox/3.8";
  private static final URI STOCKQUOTE_URI;

  static {
    try {
      STOCKQUOTE_URI =
      new URI("http://www.stockbangladesh.com/getpricelist.php");
    }
    catch (Exception ex) {
      throw new IllegalArgumentException(ex);
    }
  }
  private static final Client CLIENT = Client.create();

  static {
  }
  private static final Logger log = Logger.getLogger(
          StockBangladeshDSEClient.class.getName());

  public static PingResult getLatestQuotes() {
    WebResource resource = CLIENT.resource(STOCKQUOTE_URI);
    resource.accept(new MediaType[] {MediaType.APPLICATION_JSON_TYPE});
    resource.header("User-Agent", FF_UA);
    Object response = resource.post(String.class);
    String stringResponse = response.toString();
    stringResponse = stringResponse.substring(1, stringResponse.length() - 1);
    ObjectMapper mapper = new ObjectMapper();
    StringReader reader = new StringReader(stringResponse);
    PingResult result = null;
    try {
      log.finest("Getting DSE data from StockBangladesh");
      result = (PingResult) mapper.readValue(reader, PingResult.class);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return result;
  }
}
