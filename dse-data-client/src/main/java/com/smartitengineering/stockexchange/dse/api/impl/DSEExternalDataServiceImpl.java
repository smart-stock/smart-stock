package com.smartitengineering.stockexchange.dse.api.impl;

import com.google.inject.Singleton;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.ExternalDataService;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import com.smartitengineering.stockexchange.dse.data.PingResult;
import com.smartitengineering.stockexchange.dse.data.StockBangladeshDSEClient;
import com.smartitengineering.stockexchange.dse.data.StockBangladeshDSEQuote;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imyousuf
 */
@Singleton
public class DSEExternalDataServiceImpl
        implements ExternalDataService {

  public static final String COMPANY_PAGE_PREFIX =
                             "http://www.stockbangladesh.com/company_details.php?code=";
  public static final String DSE_BD_SYMBOL = "dse_bd";
  public static final String CSE_BD_SYMBOL = "cse_bd";
  private static final Logger log = Logger.getLogger(
          DSEExternalDataServiceImpl.class.getName());

  public Collection<StockQuote> getLatestLiveStockQuotes() {
    PingResult result = StockBangladeshDSEClient.getLatestQuotes();
    Set<StockQuote> quotes = new HashSet<StockQuote>(result.getTotal());
    if (result.getTotal() > 0) {
      StockBangladeshDSEQuote[] rawQuotes = result.getResults();
      if (rawQuotes != null && rawQuotes.length > 0) {
        Date quoteDate = new Date();
        for (StockBangladeshDSEQuote rawQuote : rawQuotes) {
          StockQuote quote = new StockQuote();
          Company company = new Company();
          quote.setCompany(company);
          quote.setChangePercentage(rawQuote.getPchange());
          quote.setHigh(rawQuote.getHigh());
          quote.setLastPrice(rawQuote.getLastprice());
          quote.setLow(rawQuote.getLow());
          quote.setQuoteTime(quoteDate);
          quote.setTrade(rawQuote.getTrade());
          quote.setValue(rawQuote.getValue());
          quote.setVolume(rawQuote.getVolume());
          company.setCategory(rawQuote.getCategory());
          company.setDescription("");
          company.setExchange(getRawStockExchange());
          try {
            company.setFaceValue(Double.parseDouble(rawQuote.getFace_value()));
          }
          catch (Exception exception) {
            if (log.isLoggable(Level.WARNING)) {
              log.log(Level.WARNING, new StringBuilder(
                      "Exception in converting faceValue: ").append(exception.
                      getMessage()).append(". Turn on FINER to get more info!").
                      toString());
            }
            log.log(Level.FINER, exception.getMessage(), exception);
          }
          try {
            company.setLotSize(Integer.parseInt(rawQuote.getMarket_lot()));
          }
          catch (Exception exception) {
            if (log.isLoggable(Level.WARNING)) {
              log.log(Level.WARNING, new StringBuilder(
                      "Exception in converting lotSize: ").append(exception.
                      getMessage()).append(". Turn on FINER to get more info!").
                      toString());
            }
            log.log(Level.FINER, exception.getMessage(), exception);
          }
          company.setSector(rawQuote.getSector());
          company.setSymbol(rawQuote.getCode());
          quotes.add(quote);
        }
      }
    }
    return quotes;
  }

  public String getStockExchangeSymbol() {
    return DSE_BD_SYMBOL;
  }

  public StockExchange getRawStockExchange() {
    StockExchange exchange = new StockExchange();
    exchange.setName("Dhaka Stock Exchange");
    exchange.setSymbolicName(getStockExchangeSymbol());
    exchange.setAddress(
            "Stock Exchange Building, 9/F Motijheel C/A, Dhaka, Bangladesh");
    exchange.setWebsiteUrl("http://www.dsebd.org/");
    exchange.setTradingEndTime("15:00");
    exchange.setTradingStartTime("11:00");
    return exchange;
  }

  public String getCompanyUrl(Company company) {
    if (company == null || company.getSymbol() == null) {
      return "";
    }
    return new StringBuilder(COMPANY_PAGE_PREFIX).append(company.getSymbol()).
            toString();
  }
}
