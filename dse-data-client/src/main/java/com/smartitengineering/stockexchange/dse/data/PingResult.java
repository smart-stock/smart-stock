package com.smartitengineering.stockexchange.dse.data;

/**
 *
 * @author imyousuf
 */
public class PingResult {
  private int total;
  private StockBangladeshDSEQuote[] results;

  public StockBangladeshDSEQuote[] getResults() {
    return results;
  }

  public void setResults(StockBangladeshDSEQuote[] results) {
    this.results = results;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }
}
